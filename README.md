# packer-virtualbox-demo

Virtualbox RHEL Stig Demo, includes code example for Hyper-V

## Getting started

Generate user and root passwords to use in the following files:
```
http/rhel7.cfg
rhel7-virtualbox.json.pkr.hcl
```

Use the `openssl passwd -5` command to generate a default password.
Replace <GENERATED_ROOT_PASSWORD> with the root password.
Replace <GENERATED_USER_PASSWORD> with the default admin users password where applicable.

In rhel7.cfg you will also need to replace YOURORG and YOURACTIVATIONKEY with the bits from your own redhat account.  Free developer licenses are avialble and renewable through them for learning and home labs.

https://access.redhat.com/labs/kickstartconfig/ can help build a rhel7.cfg if this one is not what you want or you want to update to a new one. Rhel8 perhaps.


once those values have been replaced, you should be able to run:

<pathtopacker>`packer build rhel7-virtualbox.pkr.hcl`
